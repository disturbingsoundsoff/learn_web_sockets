package main

import (
	"fmt"
	"log"
	"net/http"
	"strconv"
	"strings"

	"github.com/gorilla/websocket"
	"gitlab.com/disturbingsoundsoff/learn_web_sockets/game"
)

const PORT = 8080

const (
    CREATE_GAME = "create_game"
    JOIN_GAME = "join_game"
    GAME_ID = "game_id"
)

var upgrader = websocket.Upgrader{}
var games []game.Game

func main() {
    http.Handle("/", http.FileServer(http.Dir("./frontend")))

    http.HandleFunc("/game", func(w http.ResponseWriter, r *http.Request) {
        conn, err := upgrader.Upgrade(w, r, nil)
        if err != nil {
            log.Println("upgrade failed: ",err)
            return
        }
        defer conn.Close()

        for {
            typeOfMessage, byteMessage, err := conn.ReadMessage()
            if err != nil {
                log.Println("read failed: ",err)
                break
            }
            input := string(byteMessage)
            inputArr := strings.Split(input, ";")
            serverCmd := inputArr[0]

            if serverCmd == CREATE_GAME {
                log.Println("got message to create a game")
                gamesCount := len(games)
                games = append(games, game.NewGame(gamesCount, conn, typeOfMessage))
                output := fmt.Sprintf("new_game_id %d;player_id 0", gamesCount)
                byteMessage = []byte(output)
                err = conn.WriteMessage(typeOfMessage, byteMessage)
                if err != nil {
                    log.Printf("write failed: %v\n",err)
                    break
                }
            } else if serverCmd == JOIN_GAME {
                
            } else {
                serverGameCmd := strings.Split(serverCmd, " ")
                if serverGameCmd[0] == GAME_ID {
                    id, err := strconv.Atoi(serverGameCmd[1])
                    if err != nil {
                        log.Println("failed to parse game id: ",err)
                        break
                    }
                    gameMessage := inputArr[1]
                    games[id].HandleMessage(gameMessage)
                }
            }


            // byteMessage = []byte(output)
            // err = conn.WriteMessage(typeOfMessage, byteMessage)
            // if err != nil {
            //     log.Printf("write failed: %v\n",err)
            //     break
            // }
        }
    })
    fmt.Printf("Listening on localhost:%d", PORT)
    http.ListenAndServe(fmt.Sprintf(":%d", PORT), nil)
}
