package utils

func Contains(slice []string, str string) bool  {
    for _, v := range slice {
        if v == str {return true}
    }
    return false
}

func Remove(slice []string, str string) []string {
    rez := []string{}
    for _, v := range slice {
        if v != str {
            rez = append(rez, v)
        }
    }
    return rez
}
