package game

import (
	"log"
	"strconv"
	"strings"

	"github.com/gorilla/websocket"
	"gitlab.com/disturbingsoundsoff/learn_web_sockets/utils"
)

const (
    PLAYER_ID = "player_id"
    KEY_DOWN = "key_down"
    KEY_UP = "key_up"
)

type Player struct {
    id int
    game *Game
    collisionX float64
    collisionY float64 
    collisionRadius int
    speed float64 
    pressedKeys []string
}
func newPlayer(id int, game *Game, collisionX, collisionY float64) Player {
    return Player{
        id: id,
        game: game,
        collisionX: collisionX,
        collisionY: collisionY,
        collisionRadius: 40,
        speed: 8,
    }
}

func (player *Player) move() {
    direction := struct { 
        vertical float64 
        horizontal float64 
    }{ vertical: 0, horizontal: 0,}
    
    if utils.Contains(player.pressedKeys, "KeyW") { direction.vertical -=1 }
    if utils.Contains(player.pressedKeys, "KeyS") { direction.vertical +=1 }
    if utils.Contains(player.pressedKeys, "KeyA") { direction.horizontal -=1 }
    if utils.Contains(player.pressedKeys, "KeyD") { direction.horizontal +=1 }

    speed := player.speed
    if (direction.horizontal != 0 && direction.vertical != 0) {
        speed *= 0.71
    }
    player.collisionX += direction.horizontal * speed

}

type Monster struct {

}

type Game struct {
    id int
    players []Player
    // monsters []Monster
    // currentMap string
    width float64
    height float64
    connection *websocket.Conn
    typeOfMessage int
}

func NewGame(id int, connection *websocket.Conn, typeOfMessage int) Game {
    game := Game{
        id: id,
        connection: connection,
        typeOfMessage: typeOfMessage,
    }
    game.players = []Player{newPlayer(0, &game, 100, 100)}

    // message := 
    // connection.WriteMessage()
    
    return game
}

// func (game *Game) checkMonsterCollision() {
//
// }

func (game *Game) Update(clientMessage string) {
    for _, player := range game.players {
        player.move()
    }
}

func (game *Game) HandleMessage(message string) {
    log.Println(message)
    data := strings.Split(message, ":")
    id, err := strconv.Atoi( strings.Split(data[0], " ")[1] )
    if err != nil {
        log.Println("failed to parse game id: ",err)
        return 
    }
    if strings.Contains(data[1], KEY_DOWN) {
        key := strings.Split(data[1], " ")[1]
        game.players[id].pressedKeys = append(game.players[id].pressedKeys, key)
    } else if strings.Contains(data[1], KEY_UP) {
        key := strings.Split(data[1], " ")[1]
        game.players[id].pressedKeys = utils.Remove(game.players[id].pressedKeys, key)
    }
    
    log.Printf("%+v", game.players[id])
}
