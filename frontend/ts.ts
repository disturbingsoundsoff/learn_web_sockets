const canvas: HTMLCanvasElement = document.getElementById("myCanvas") as HTMLCanvasElement
// canvas.width = window.innerWidth;
// canvas.height = window.innerHeight;
canvas.width = 1920;
canvas.height = 1080;
const ctx = canvas!.getContext("2d")!;

ctx.fillStyle = "orange";
ctx.lineWidth = 3;
ctx.strokeStyle = "orange";


class Player {
    id: number
    game: Game;
    collisionX: number;
    collisionY: number;
    collisionRadius: number;
    speed: number;

    constructor(game: Game) {
        this.game = game;
        this.collisionX = this.game.width / 2;
        this.collisionY = this.game.height / 2;
        this.collisionRadius = 40;
        this.speed = 8;
    }

    draw(context: CanvasRenderingContext2D) {
        context.beginPath();
        context.arc(
            this.collisionX, 
            this.collisionY, 
            this.collisionRadius, 
            0, 
            Math.PI * 2
        );
        context.save();
        context.globalAlpha = 0.5;
        context.fill();
        context.restore();
        context.stroke();
    }

    move(keysPressed: string[]) {
        const direction = {vertical: 0, horizontal: 0};

        if (keysPressed.indexOf("KeyW") != -1) { direction.vertical -= 1}
        if (keysPressed.indexOf("KeyS") != -1) { direction.vertical += 1}
        if (keysPressed.indexOf("KeyA") != -1) { direction.horizontal -= 1}
        if (keysPressed.indexOf("KeyD") != -1) { direction.horizontal += 1}

        let speed = this.speed;
        if (direction.horizontal != 0 && direction.vertical != 0) {
            speed *= 0.71;
        }
        this.collisionX += direction.horizontal * speed;
        this.collisionY += direction.vertical * speed;
    }
}

class Game {
    id: number
    canvas: HTMLCanvasElement;
    width: number;
    height: number;
    player: Player;
    pressedKeys: string[];
    socket: WebSocket

    constructor(canvas: HTMLCanvasElement, socket: WebSocket){
        this.canvas = canvas;
        this.width = canvas.width;
        this.height = canvas.height;
        this.player = new Player(this);
        this.pressedKeys = [];
        this.socket = socket;

        window.onkeydown = (event: KeyboardEvent) => {
            // if (event.repeat) { return; }
            // this.pressedKeys.push(event.code);
            // console.log(this.pressedKeys);
            this.socket.send(`game_id ${this.id};player_id 0:key_down ${event.code}`)
        }

        window.onkeyup = (event: KeyboardEvent) => {
            // const index = this.pressedKeys.indexOf(event.code);
            // if (index > -1) {
            //     this.pressedKeys.splice(index, 1);
            // }
        }
    }

    render(context: CanvasRenderingContext2D) {
        this.player.draw(context);
        this.player.move(this.pressedKeys);
    }

}


const socket = new WebSocket("ws://localhost:8080/game");

const game = new Game(canvas, socket);

socket.onopen = () => {
    socket.send("create_game")
};

socket.onmessage = (e) => {
    console.log(e.data)
    const data: string = e.data
    if (data.includes("new_game_id")) {
        const [game_data, player_data] = data.split(";")
        game.id = Number(game_data.split(" ")[1]) || 0 
        game.player.id = Number(player_data.split(" ")[1]) || 0
    }
} 



// function animate() {
//     ctx.clearRect(0, 0, canvas.width, canvas.height);
//     game.render(ctx);
//     window.requestAnimationFrame(animate);
// }

// animate();
