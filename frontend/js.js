// frontend/ts.ts
var canvas = document.getElementById("myCanvas");
canvas.width = 1920;
canvas.height = 1080;
var ctx = canvas.getContext("2d");
ctx.fillStyle = "orange";
ctx.lineWidth = 3;
ctx.strokeStyle = "orange";

class Player {
  id;
  game;
  collisionX;
  collisionY;
  collisionRadius;
  speed;
  constructor(game) {
    this.game = game;
    this.collisionX = this.game.width / 2;
    this.collisionY = this.game.height / 2;
    this.collisionRadius = 40;
    this.speed = 8;
  }
  draw(context) {
    context.beginPath();
    context.arc(this.collisionX, this.collisionY, this.collisionRadius, 0, Math.PI * 2);
    context.save();
    context.globalAlpha = 0.5;
    context.fill();
    context.restore();
    context.stroke();
  }
  move(keysPressed) {
    const direction = { vertical: 0, horizontal: 0 };
    if (keysPressed.indexOf("KeyW") != -1) {
      direction.vertical -= 1;
    }
    if (keysPressed.indexOf("KeyS") != -1) {
      direction.vertical += 1;
    }
    if (keysPressed.indexOf("KeyA") != -1) {
      direction.horizontal -= 1;
    }
    if (keysPressed.indexOf("KeyD") != -1) {
      direction.horizontal += 1;
    }
    let speed = this.speed;
    if (direction.horizontal != 0 && direction.vertical != 0) {
      speed *= 0.71;
    }
    this.collisionX += direction.horizontal * speed;
    this.collisionY += direction.vertical * speed;
  }
}

class Game {
  id;
  canvas;
  width;
  height;
  player;
  pressedKeys;
  socket;
  constructor(canvas2, socket) {
    this.canvas = canvas2;
    this.width = canvas2.width;
    this.height = canvas2.height;
    this.player = new Player(this);
    this.pressedKeys = [];
    this.socket = socket;
    window.onkeydown = (event) => {
      this.socket.send(`game_id ${this.id};key_down ${event.code}`);
    };
    window.onkeyup = (event) => {
    };
  }
  render(context) {
    this.player.draw(context);
    this.player.move(this.pressedKeys);
  }
}
var socket = new WebSocket("ws://localhost:8080/game");
var game = new Game(canvas, socket);
socket.onopen = () => {
  socket.send("create_game");
};
socket.onmessage = (e) => {
  console.log(e.data);
  const data = e.data;
  if (data.includes("new_game_id")) {
    const [game_data, player_data] = data.split(";");
    game.id = Number(game_data.split(" ")[1]) || 0;
  }
};
